package com.yupi.springbootinit.utils;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.json.JSONObject;
import com.alibaba.otter.canal.client.CanalConnector;
import com.alibaba.otter.canal.client.CanalConnectors;
import com.alibaba.otter.canal.protocol.CanalEntry;
import com.alibaba.otter.canal.protocol.Message;
import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.xpand.starter.canal.annotation.CanalEventListener;
import com.xpand.starter.canal.annotation.ListenPoint;
import com.yupi.springbootinit.esdao.PostEsDao;
import com.yupi.springbootinit.model.dto.post.PostEsDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.net.InetSocketAddress;
import java.util.List;

/**
 * canal 同步mysql 数据到 es
 */
@Slf4j
// TODO: 2023/8/14 取消注解开启canal监听数据
//@Component
//@CanalEventListener
public class CanalClientToEs {

    @Resource
    private PostEsDao postEsDao;

    @ListenPoint(destination = "监听MySQL数据", schema = "my_search_db", table = {"post"}, eventType = {CanalEntry.EventType.UPDATE, CanalEntry.EventType.DELETE, CanalEntry.EventType.INSERT})
    public void incSyncMySqlDataToEs() {
        //创建连接
        InetSocketAddress address = new InetSocketAddress("39.106.132.140", 3306);//canal 服务
        CanalConnector connector = CanalConnectors.newSingleConnector(address, "使用canal账户监听MySQL数据", "canal", "canal");
        while (true) {
            final int getDataCount = 4000;
            //连接
            connector.connect();
            //订阅数据库
            connector.subscribe("my_search_db.post");
            //获取数据
            Message message = connector.get(getDataCount);
            //获取 entrie 集合
            List<CanalEntry.Entry> entries = message.getEntries();
            if (CollUtil.isNotEmpty(entries)) {
                //遍历 entry 解析单条数据
                for (CanalEntry.Entry entry : entries) {
                    //获取表名
                    String tableName = entry.getHeader().getTableName();
                    //获取类型
                    CanalEntry.EntryType entryType = entry.getEntryType();
                    //获取序列化后的数据
                    ByteString storeValue = entry.getStoreValue();
                    //判断 entryType 类型是否为行数据
                    if (CanalEntry.EntryType.ROWDATA.equals(entryType)) {
                        //反序列化数据
                        CanalEntry.RowChange rowChange = null;
                        try {
                            rowChange = CanalEntry.RowChange.parseFrom(storeValue);
                        } catch (InvalidProtocolBufferException e) {
                            log.error("反序列化失败 storeValue = {} ex = {}", storeValue, e.getMessage());
                        }
                        //时间操作类型
                        CanalEntry.EventType eventType = rowChange.getEventType();
                        //获取变更数据
                        List<CanalEntry.RowData> rowDataList = rowChange.getRowDatasList();
                        for (CanalEntry.RowData rowData : rowDataList) {
//                            //数据改变前
//                            JSONObject beforeData = new JSONObject();
//                            List<CanalEntry.Column> beforeColumnsList = rowData.getBeforeColumnsList();
//                            for (CanalEntry.Column column : beforeColumnsList) {
//                                beforeData.set(column.getName(), column.getValue());
//                            }
//                            log.info("table = {} EventType = {} BeforeData = {}", tableName, eventType, beforeData);

                            //数据改变后
                            JSONObject afterData = new JSONObject();
                            List<CanalEntry.Column> afterColumnsList = rowData.getAfterColumnsList();
                            for (CanalEntry.Column column : afterColumnsList) {
                                afterData.set(column.getName(), column.getValue());
                            }
                            log.info("table = {} EventType = {} AfterData = {}", tableName, eventType, afterData);
                            //同步es
                            PostEsDTO postEsDTO = BeanUtil.copyProperties(afterData, PostEsDTO.class);
                            postEsDao.save(postEsDTO);
                        }
                    } else {
                        log.info("当前操作类型 = {}", entryType);
                    }
                }
            }
        }
    }
}