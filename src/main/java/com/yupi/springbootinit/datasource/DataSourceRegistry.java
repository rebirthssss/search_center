package com.yupi.springbootinit.datasource;

import com.yupi.springbootinit.model.enums.SearchTypeEnum;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * @author ssh
 * @create 2023/8/3 22:37
 *
 * 注册器DataSource实例获取
 */

@Component
public class DataSourceRegistry {

    @Resource
    private PictureDataSource pictureDataSource;

    @Resource
    private PostDataSource postDataSource;

    @Resource
    private UserDataSource userDataSource;

    @Resource
    private VideoDataSource videoDataSource;

    private Map<String, DataSource<T>> typeDataSourceMap;

    @PostConstruct
    public void doInit() {
        typeDataSourceMap = new HashMap(){{
            put(SearchTypeEnum.POST_TYPE.getValue(), postDataSource);
            put(SearchTypeEnum.USER_TYPE.getValue(), userDataSource);
            put(SearchTypeEnum.PICTURE_TYPE.getValue(), pictureDataSource);
            put(SearchTypeEnum.VIDEO_TYPE.getValue(), videoDataSource);
        }};
    }

    public DataSource getDataSourceByType(String type) {
        if (typeDataSourceMap == null) {
            return null;
        }
        return typeDataSourceMap.get(type);
    }
}
